package io.github.densyakun.domination;

import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.projectiles.ProjectileSource;

import java.util.*;

import static io.github.densyakun.domination.Domination.*;

public class DominationGame implements Runnable, Listener {

    public static int POINT_TIME = 5 * 20;
    public static int MAX_POINT = 100;
    public static int TAKING_TIME = 5 * 20;

    public static DominationGame game;

    public Domination dm;
    public List<Player> redPlayingPlayers;
    public List<Player> bluePlayingPlayers;
    public boolean started;
    public int time;
    public int redPoint;
    public int bluePoint;
    public boolean teamChanged;
    public Map<Block, Integer> redOnFlag;
    public Map<Block, Integer> blueOnFlag;
    public Map<Block, Integer> flagPoints;

    public DominationGame(Domination dm, List<Player> redPlayers, List<Player> bluePlayers) {
        this.dm = dm;
        redPlayingPlayers = new ArrayList<>(redPlayers);
        bluePlayingPlayers = new ArrayList<>(bluePlayers);
        time = teamChangeTime * 20;

        redOnFlag = new HashMap<>();
        blueOnFlag = new HashMap<>();
        flagPoints = new HashMap<>();
        flags.forEach(l -> {
            redOnFlag.put(l.getBlock(), 0);
            blueOnFlag.put(l.getBlock(), 0);
            flagPoints.put(l.getBlock(), 0);
        });

        game = this;
    }

    @Override
    public void run() {
        Bukkit.getScheduler().runTask(dm, () -> {
            flags.forEach(l -> l.getBlock().setType(Material.WHITE_WOOL));
            resetChests();

            playWorld.getEntitiesByClasses(Item.class).iterator().forEachRemaining(Entity::remove);

            redPlayingPlayers.forEach(player -> {
                player.getInventory().clear();
                initItem(player.getInventory());
                player.sendMessage(msg(ChatColor.GREEN + "あなたは " + ChatColor.RED + "赤チーム" + ChatColor.GREEN + " です"));
            });
            bluePlayingPlayers.forEach(player -> {
                player.getInventory().clear();
                initItem(player.getInventory());
                player.sendMessage(msg(ChatColor.GREEN + "あなたは " + ChatColor.BLUE + "青チーム" + ChatColor.GREEN + " です"));
            });

            Bukkit.getPluginManager().registerEvents(this, dm);
            respawn();
        });
    }

    public void initItem(Inventory i) {
        for (Material k : initItemAmount.keySet())
            i.addItem(new ItemStack(k, initItemAmount.get(k)));
    }

    public void resetChests() {
        for (Location l : chestLocations) {
            try {
                Inventory i = ((Chest) l.getBlock().getState()).getInventory();
                i.clear();

                double a1 = new Random().nextDouble() * chestAmountsSum;
                double a2 = 0.0;
                for (int k : chestAmounts.keySet()) {
                    if (a1 < a2 + chestAmounts.get(k)) {
                        for (int a3 = 0; a3 < k; a3++) {
                            a1 = new Random().nextDouble() * categoryChanceSum;
                            a2 = 0.0;
                            for (String k1 : categoryChance.keySet()) {
                                if (a1 < a2 + categoryChance.get(k1)) {
                                    Map<Material, Double> c = itemChance.get(k1);
                                    a1 = new Random().nextDouble() * itemChanceSum.get(k1);
                                    a2 = 0.0;
                                    for (Material k2 : c.keySet()) {
                                        if (a1 < a2 + c.get(k2)) {
                                            i.addItem(new ItemStack(k2, itemAmount.get(k1).get(k2)));
                                            break;
                                        }
                                        a2 += c.get(k2);
                                    }
                                    break;
                                }
                                a2 += categoryChance.get(k1);
                            }
                        }
                        break;
                    }
                    a2 += chestAmounts.get(k);
                }

                List<ItemStack> c = Arrays.asList(i.getContents());
                Collections.shuffle(c);
                i.setContents(c.toArray(new ItemStack[0]));
            } catch (NullPointerException e) {
                Bukkit.broadcastMessage(msg("チェストが見つかりません: " + l));
            }
        }
    }

    public void respawn(Player player) {
        if (redPlayingPlayers.contains(player)) {
            player.setDisplayName(ChatColor.RED + player.getName());
            player.setPlayerListName(ChatColor.RED + player.getName());
            if (teamChanged)
                player.teleport(blueSpawnPoint);
            else
                player.teleport(redSpawnPoint);
            player.setHealth(player.getHealthScale());
        } else if (bluePlayingPlayers.contains(player)) {
            player.setDisplayName(ChatColor.BLUE + player.getName());
            player.setPlayerListName(ChatColor.BLUE + player.getName());
            if (teamChanged)
                player.teleport(redSpawnPoint);
            else
                player.teleport(blueSpawnPoint);
            player.setHealth(player.getHealthScale());
        }
    }

    public void respawn() {
        redPlayingPlayers.forEach(player -> {
            player.setDisplayName(ChatColor.RED + player.getName());
            player.setPlayerListName(ChatColor.RED + player.getName());
            player.setHealth(player.getHealthScale());
            if (teamChanged)
                player.teleport(blueSpawnPoint);
            else
                player.teleport(redSpawnPoint);
        });
        bluePlayingPlayers.forEach(player -> {
            player.setDisplayName(ChatColor.BLUE + player.getName());
            player.setPlayerListName(ChatColor.BLUE + player.getName());
            player.setHealth(player.getHealthScale());
            if (teamChanged)
                player.teleport(redSpawnPoint);
            else
                player.teleport(blueSpawnPoint);
        });

        Bukkit.getScheduler().runTaskAsynchronously(dm, () -> {
            for (int i = Domination.countDown; i > 0; i--) {
                Bukkit.broadcastMessage(msg(ChatColor.GREEN + "ゲーム開始まで " + ChatColor.GOLD + i + ChatColor.GREEN + "秒"));
                Bukkit.getOnlinePlayers().forEach(player -> player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_HARP, 1f, 1f));
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            started = true;
            Bukkit.broadcastMessage(msg(ChatColor.GREEN + "ゲーム開始！"));
            Bukkit.getOnlinePlayers().forEach(player -> player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_PLACE, 1f, 0.5f));

            if (game != null)
                Bukkit.getScheduler().runTaskLater(dm, this::tick, 1);
        });
    }

    public void tick() {
        time--;

        flagPoints.keySet().forEach(block -> {
            if (redOnFlag.get(block) > 0 || blueOnFlag.get(block) > 0) {
                int a = redOnFlag.get(block) - blueOnFlag.get(block);
                int b = flagPoints.get(block);
                int p = Math.max(Math.min(b + a, TAKING_TIME), -TAKING_TIME);
                flagPoints.put(block, p);
                if (p == TAKING_TIME)
                    block.setType(Material.RED_WOOL);
                else if (p == -TAKING_TIME)
                    block.setType(Material.BLUE_WOOL);
                else if (a > 0 && b < 0 && p >= 0 ||
                        a < 0 && b > 0 && p <= 0)
                    block.setType(Material.WHITE_WOOL);
                if (a > 0 && b != TAKING_TIME ||
                        a < 0 && b != -TAKING_TIME)
                    Bukkit.getOnlinePlayers().forEach(player -> player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 1f, 1f + (float) p / TAKING_TIME));
            }
        });

        if (time % POINT_TIME == 0)
            countPoint();
        else
            Bukkit.getScheduler().runTaskLater(dm, this::tick, 1);
    }

    public void countPoint() {
        if (time <= 0) {
            time = teamChangeTime * 20;
            changeTeam();
        } else {
            boolean a = false;
            for (Location l : flags) {
                switch (l.getBlock().getType()) {
                    case RED_WOOL:
                        a = true;
                        redPoint++;
                        break;
                    case BLUE_WOOL:
                        a = true;
                        bluePoint++;
                        break;
                }
            }
            if (a)
                Bukkit.broadcastMessage(msg(ChatColor.RED + "赤: " + redPoint + ChatColor.BLUE + " 青: " + bluePoint));
            if (redPoint >= MAX_POINT || bluePoint >= MAX_POINT)
                endGame();
            else
                Bukkit.getScheduler().runTaskLater(dm, this::tick, 1);
        }
    }

    public void changeTeam() {
        started = false;
        teamChanged = !teamChanged;
        redOnFlag = new HashMap<>();
        blueOnFlag = new HashMap<>();
        flagPoints = new HashMap<>();
        flags.forEach(l -> {
            redOnFlag.put(l.getBlock(), 0);
            blueOnFlag.put(l.getBlock(), 0);
            flagPoints.put(l.getBlock(), 0);
        });
        flags.forEach(l -> l.getBlock().setType(Material.WHITE_WOOL));

        Bukkit.broadcastMessage(msg(ChatColor.GREEN + "陣営交代！"));
        Bukkit.getOnlinePlayers().forEach(player -> player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_PLACE, 1f, 0.5f));

        respawn();
    }

    public void endGame() {
        HandlerList.unregisterAll(this);
        Bukkit.getScheduler().cancelTasks(dm);
        game = null;

        Bukkit.broadcastMessage(msg(ChatColor.GOLD + "ゲームが終了しました！"));
        Bukkit.getOnlinePlayers().forEach(player -> player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_PLACE, 1f, 1f));
    }

    @EventHandler
    public void onBlockDamage(BlockDamageEvent e) {
        if (!started)
            e.setCancelled(true);
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        Location from = e.getFrom();
        Location to = Objects.requireNonNull(e.getTo());
        if (started) {
            Block fromFlag = Objects.requireNonNull(from.getWorld()).getBlockAt(from.getBlockX(), from.getBlockY() - 1, from.getBlockZ());
            Block toFlag = Objects.requireNonNull(to.getWorld()).getBlockAt(to.getBlockX(), to.getBlockY() - 1, to.getBlockZ());
            if (fromFlag.getType() == Material.WHITE_WOOL ||
                    fromFlag.getType() == Material.RED_WOOL ||
                    fromFlag.getType() == Material.BLUE_WOOL) {
                if (flags.contains(fromFlag.getLocation()))
                    if (redPlayingPlayers.contains(e.getPlayer()))
                        redOnFlag.put(fromFlag, redOnFlag.get(fromFlag) - 1);
                    else
                        blueOnFlag.put(fromFlag, blueOnFlag.get(fromFlag) - 1);
            }
            if (toFlag.getType() == Material.WHITE_WOOL ||
                    toFlag.getType() == Material.RED_WOOL ||
                    toFlag.getType() == Material.BLUE_WOOL) {
                if (flags.contains(toFlag.getLocation()))
                    if (redPlayingPlayers.contains(e.getPlayer()))
                        redOnFlag.put(toFlag, redOnFlag.get(toFlag) + 1);
                    else
                        blueOnFlag.put(toFlag, blueOnFlag.get(toFlag) + 1);
            }
        } else {
            from.setYaw(to.getYaw());
            from.setPitch(to.getPitch());
            e.setTo(from);
        }
    }

    @EventHandler
    public void onPlayerDamage(EntityDamageEvent e) {
        if (e.getEntity() instanceof Player) {
            if (!started)
                e.setCancelled(true);
            else if (0 >= Math.floor(((Player) e.getEntity()).getHealth() - e.getDamage()))
                respawn((Player) e.getEntity());
        }
    }

    @EventHandler
    public void onPlayerDamageByEntity(EntityDamageByEntityEvent e) {
        if (started) {
            Entity p = e.getEntity();
            if (p instanceof Player) {
                Entity d = e.getDamager();
                if (d instanceof Player) {
                    if (redPlayingPlayers.contains(p) == redPlayingPlayers.contains(d))
                        e.setCancelled(true);
                } else if (d instanceof Projectile) {
                    ProjectileSource s = ((Projectile) d).getShooter();
                    if (s instanceof Player && redPlayingPlayers.contains(p) == redPlayingPlayers.contains(s))
                        e.setCancelled(true);
                }
            }
        } else
            e.setCancelled(true);
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        respawn(e.getPlayer());
    }
}
