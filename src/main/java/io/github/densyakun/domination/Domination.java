package io.github.densyakun.domination;

import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.*;

public final class Domination extends JavaPlugin {

    private static String prefix;

    public static int countDown;
    public static int teamChangeTime;
    public static World playWorld;
    public static Location redSpawnPoint;
    public static Location blueSpawnPoint;

    public static Map<Material, Integer> initItemAmount;

    public static List<Location> flags;

    public static List<Location> chestLocations;
    public static Map<Integer, Double> chestAmounts;
    public static double chestAmountsSum;
    public static Map<String, Double> categoryChance;
    public static double categoryChanceSum;
    public static Map<String, Map<Material, Integer>> itemAmount;
    public static Map<String, Map<Material, Double>> itemChance;
    public static Map<String, Double> itemChanceSum;

    public static List<Player> redPlayers = new ArrayList<>();
    public static List<Player> bluePlayers = new ArrayList<>();

    @Override
    public void onEnable() {
        prefix = ChatColor.GOLD + "" + ChatColor.BOLD + "[Dm] ";

        saveDefaultConfig();
        FileConfiguration conf = getConfig();
        countDown = conf.getInt("countDown");
        teamChangeTime = conf.getInt("teamChangeTime");
        playWorld = getServer().getWorld(Objects.requireNonNull(conf.getString("playWorld")));

        redSpawnPoint = new Location(
                playWorld,
                conf.getDouble("redSpawnPoint.x"),
                conf.getDouble("redSpawnPoint.y"),
                conf.getDouble("redSpawnPoint.z"),
                Float.parseFloat(conf.getDouble("redSpawnPoint.yaw") + ""),
                Float.parseFloat(conf.getDouble("redSpawnPoint.pitch") + "")
        );
        blueSpawnPoint = new Location(
                playWorld,
                conf.getDouble("blueSpawnPoint.x"),
                conf.getDouble("blueSpawnPoint.y"),
                conf.getDouble("blueSpawnPoint.z"),
                Float.parseFloat(conf.getDouble("blueSpawnPoint.yaw") + ""),
                Float.parseFloat(conf.getDouble("blueSpawnPoint.pitch") + "")
        );

        initItemAmount = new HashMap<>();
        ConfigurationSection i = conf.getConfigurationSection("initItems");
        for (String s1 : Objects.requireNonNull(i).getKeys(false))
            initItemAmount.put(Material.valueOf(s1), i.getInt(s1 + ".amount", 1));

        flags = new ArrayList<>();
        conf.getMapList("flags").forEach(map -> flags.add(new Location(
                playWorld,
                (Integer) map.get("x"),
                (Integer) map.get("y"),
                (Integer) map.get("z")
        )));

        ConfigurationSection chest = conf.getConfigurationSection("chest");
        if (chest != null) {
            chestLocations = new ArrayList<>();
            chest.getMapList("locations").forEach(map -> chestLocations.add(new Location(
                    playWorld,
                    (Integer) map.get("x"),
                    (Integer) map.get("y"),
                    (Integer) map.get("z")
            )));
            chestAmounts = new HashMap<>();
            chestAmountsSum = 0.0;
            double v;
            for (String s : Objects.requireNonNull(chest.getConfigurationSection("amounts")).getKeys(false)) {
                v = chest.getDouble("amounts." + s);
                chestAmounts.put(Integer.parseInt(s), v);
                chestAmountsSum += v;
            }
            categoryChance = new HashMap<>();
            categoryChanceSum = 0.0;
            itemAmount = new HashMap<>();
            itemChance = new HashMap<>();
            itemChanceSum = new HashMap<>();
            for (String s : Objects.requireNonNull(chest.getConfigurationSection("categories")).getKeys(false)) {
                ConfigurationSection conf1 = chest.getConfigurationSection("categories." + s);
                v = Objects.requireNonNull(conf1).getDouble("chance");
                categoryChance.put(s, v);
                categoryChanceSum += v;

                Map<Material, Integer> a = new HashMap<>();
                Map<Material, Double> c = new HashMap<>();
                i = conf1.getConfigurationSection("items");
                double a1 = 0.0;
                double v1;
                for (String s1 : Objects.requireNonNull(i).getKeys(false)) {
                    Material m = Material.valueOf(s1);
                    a.put(m, i.getInt(s1 + ".amount", 1));
                    v1 = i.getDouble(s1 + ".chance");
                    c.put(m, v1);
                    a1 += v1;
                }
                itemAmount.put(s, a);
                itemChance.put(s, c);
                itemChanceSum.put(s, a1);
            }
        }
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 0)
            sender.sendMessage(msg(ChatColor.RED + "dm (p|start)"));
        else if (args[0].equalsIgnoreCase("p")) {
            if (args.length == 1)
                sender.sendMessage(msg(ChatColor.RED + "dm p (add|remove|list)"));
            else if (args[1].equalsIgnoreCase("add")) {
                if (args.length < 4)
                    sender.sendMessage(msg(ChatColor.RED + "dm p add (playername) (r|b)"));
                else {
                    Player p = Bukkit.getPlayerExact(args[2]);
                    if (p == null)
                        sender.sendMessage(msg(ChatColor.RED + "プレイヤー\"" + ChatColor.GOLD + args[2] + ChatColor.RED + "\"は見つかりませんでした"));
                    else if (redPlayers.contains(p) || bluePlayers.contains(p))
                        sender.sendMessage(msg(ChatColor.RED + "プレイヤー\"" + ChatColor.GOLD + p.getName() + ChatColor.RED + "\"はすでに参加しています"));
                    else {
                        String t = args[3];
                        boolean isRed = t.equalsIgnoreCase("r");
                        if (isRed || t.equalsIgnoreCase("b")) {
                            if (isRed)
                                redPlayers.add(p);
                            else
                                bluePlayers.add(p);
                            sender.sendMessage(msg(ChatColor.AQUA + "プレイヤー\"" + ChatColor.GOLD + p.getName() + ChatColor.AQUA + "\"を" + (isRed ? ChatColor.RED + "赤" : ChatColor.BLUE + "青") + "チーム" + ChatColor.AQUA + "に追加しました"));
                        } else
                            sender.sendMessage(msg(ChatColor.RED + "チーム名が不正です。赤チームは \"r\" 、青チームは \"b\" を入力してください"));
                    }
                }
            } else if (args[1].equalsIgnoreCase("remove"))
                if (args.length == 2)
                    sender.sendMessage(msg(ChatColor.RED + "dm p remove (playername)"));
                else {
                    boolean r = true;
                    for (int i = 0; i < redPlayers.size(); i++) {
                        Player p = redPlayers.get(i);
                        if (p.getName().equalsIgnoreCase(args[2])) {
                            r = false;
                            redPlayers.remove(i);
                            sender.sendMessage(msg(ChatColor.AQUA + "プレイヤー\"" + ChatColor.GOLD + p.getName() + ChatColor.AQUA + "\"を削除しました"));
                            break;
                        }
                    }
                    if (r) {
                        for (int i = 0; i < bluePlayers.size(); i++) {
                            Player p = bluePlayers.get(i);
                            if (p.getName().equalsIgnoreCase(args[2])) {
                                r = false;
                                bluePlayers.remove(i);
                                sender.sendMessage(msg(ChatColor.AQUA + "プレイヤー\"" + ChatColor.GOLD + p.getName() + ChatColor.AQUA + "\"を削除しました"));
                                break;
                            }
                        }
                        if (r)
                            sender.sendMessage(msg(ChatColor.RED + "プレイヤー\"" + ChatColor.GOLD + args[2] + ChatColor.RED + "\"は参加していません"));
                    }
                }
            else if (args[1].equalsIgnoreCase("list")) {
                StringBuilder a = new StringBuilder(msg(ChatColor.GREEN + "プレイヤー (" + redPlayers.size() + ":" + bluePlayers.size() + "):"));
                a.append(ChatColor.RED).append("\n 赤チーム:");
                for (Player p : redPlayers)
                    a.append("\n").append(p.getName());
                a.append(ChatColor.BLUE).append("\n 青チーム:");
                for (Player p : bluePlayers)
                    a.append("\n").append(p.getName());
                sender.sendMessage(a.toString());
            } else
                sender.sendMessage(msg(ChatColor.RED + "dm p (add|remove|list)"));
        } else if (args[0].equalsIgnoreCase("start")) {
            if (DominationGame.game != null)
                sender.sendMessage(msg(ChatColor.RED + "ゲーム中です"));
            else if (redPlayers.isEmpty() || bluePlayers.isEmpty())
                sender.sendMessage(msg(ChatColor.RED + "プレイヤーを追加してください /dm p add (playername) (r|b)"));
            else
                startGame();
        } else
            sender.sendMessage(msg(ChatColor.RED + "dm (p|start)"));
        return true;
    }

    @Override
    public void onDisable() {
        if (DominationGame.game != null)
            DominationGame.game.endGame();
    }

    public static String msg(String text) {
        return prefix + text;
    }

    public void startGame() {
        Bukkit.getScheduler().runTaskAsynchronously(this, new DominationGame(this, new ArrayList<>(redPlayers), new ArrayList<>(bluePlayers)));
        redPlayers.clear();
        bluePlayers.clear();
    }
}
