# Domination

## config.yml

- `countDown` ゲーム開始、再開までの秒数
- `teamChangeTime` チームを変更する間隔の秒数
- `playWorld` 試合を行うワールド
- `redSpawnPoint:` 赤（青）チームのスポーン地点
  - `x: X座標`
  - `y: Y座標`
  - `z: Z座標`
  - `yaw: ヨー`
  - `pitch: ピッチ`
- `blueSpawnPoint:` 青（赤）チームのスポーン地点
    - `x: X座標`
    - `y: Y座標`
    - `z: Z座標`
    - `yaw: ヨー`
    - `pitch: ピッチ`

- `initItems:`
  - `マテリアル名:`
    - `amount: 個数`

- `flags:` 旗の座標
  - `- {x: X座標, y: Y座標, z: Z座標}`
  - `- {x: X座標, y: Y座標, z: Z座標}`
  - `- {x: X座標, y: Y座標, z: Z座標}`

- `chest:` チェスト設定
  - `locations:` チェストの座標の一覧
    - `- {x: X座標, y: Y座標, z: Z座標}`
  - `amounts:` チェスト内のアイテムの個数と確率の一覧
    - `個数: 確率`
  - `categories:` アイテムのカテゴリ
    - `カテゴリ名:`
      - `chance: カテゴリが選ばれる確率`
      - `items:`
        - `マテリアル名:`
          - `amount: 個数`
          - `chance: アイテムが選ばれる確率`

## コマンド

- `/dm p add (playername) (r|b)` プレイヤーを参加させる。r: red、b: blue。 例: `/dm p add Player1 r`
- `/dm start`

陣地のとり方>一定時間ブロックの上に立つ
スコア上限100
開始前にゲームモードを設定しておくこと
